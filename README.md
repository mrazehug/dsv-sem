## Sestavení a spuštění programu
Před spuštěním je potřeba vygenerovat protokoly pro kotlin spuštěním tasku `generateProto`.
Program je potřeba buildit jako `shadowJar`. Pro jar se mi nepodařilo přesvědčit gradle, aby našel main class.

Program jde spustit buď se dvěma nebo se čtyřmi argumenty.
Pokud je program spuštěn se dvěma argumenty, node se považuje za první a zvolí se leaderem. Prvním argumentem je IP adresa serveru a druhým port serveru.

Příklad spuštění programu se dvěma argumenty:
`java -jar dsv-sem-1.0-SNAPSHOT-shadow.jar localhost 5000`

Pokud je program spuštěn se čtyřmi argumenty, node se pokusí připojit za předchozí node. První a druhý argument jsou stejné jako při spuštění jen se dvěma argumenty, třetí a čtvrtý jsou potom IP adresa a port předchozí node.

Příklad spuštění programu se čtyřmi argumenty: `java -jar dsv-sem-1.0-SNAPSHOT-shadow.jar localhost 5001 localhost 5000`

## CLI
Každý uzel má CLI rozhraní, kterým jde s node komunikovat. Příkazy jsou následující:
- show - Command, který dokáže vypsat informace o uzlu. Má ještě druhý argument, kteý může nabývat následující možností
  - connections - vypíše všechna přípojení uzlu
  - conf - Vypíše informace o uzlu - adresu a uid
  - Příklad: show conf
- send - Command, co odešle zprávu. Má 2 přepínače:
  - -ruid - receiver uid - UID uzlu, pro který je zpráva určena
  - -text - text zprávy
  - Příklad: send -ruid 5d008109-4bd3-4aa6-83c2-ca6abb823a4d -text ”Hello there!”
- intercept - zapne nebo vypne mód intercept na nodu. V módu intercept se node snaží číst obsah zpráv, které přeposílá. Tento přepínač je zde z důvodu, aby šlo demonstrovat šifrovaní
- kill - zabije uzel bez ohlášení ostatním
- leave - zabije uzel, ale před tím to uzel ostatní následujícímu
