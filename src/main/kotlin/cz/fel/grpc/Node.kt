package cz.fel.grpc

import cz.fel.grpc.utils.RSA
import cz.fel.grpc.communication.Address
import cz.fel.grpc.communication.NodeService
import cz.fel.grpc.communication.NodeServiceClient
import kotlinx.coroutines.*
import java.lang.Runnable
import java.security.KeyPairGenerator
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.system.exitProcess

class Node(val address: Address, var pAddress: Address = address) : Runnable {
    var intercept: Boolean = false
    val uid: String = UUID.randomUUID().toString()
    val privateKey: String;
    val publicKey: String;

    private val cli: CLI = CLI(this)
    val nodeService = NodeService(this)
    val nodeServiceClient = NodeServiceClient(this)

    var nAddress = address
    var nnAddress = address
    var leaderAddress = address

    var time : AtomicInteger = AtomicInteger(0)

    init {
        val keyPair = KeyPairGenerator.getInstance("RSA").generateKeyPair();
        privateKey = Base64.getEncoder().encodeToString(keyPair.private.encoded)
        publicKey = Base64.getEncoder().encodeToString(keyPair.public.encoded)


        nodeService.start()
        GlobalScope.launch {
            nodeServiceClient.join()
            nodeServiceClient.publishPublicKey(uid, publicKey, mutableListOf())
            cli.run()
        }

        nodeService.blockUntilShutdown()
    }

    override fun run() {
    }

    fun sendMessage(ruid: String, suid: String, openText: String) {
        val encodedText = RSA.encryptMessage(openText, nodeServiceClient.keyRing[ruid] as String)
        runBlocking {
            nodeServiceClient.sendMessage(ruid, suid, encodedText)
        }
    }

    fun leave() {
        runBlocking {
            nodeServiceClient.leave(address)
            kill()
        }
    }

    fun kill() {
        exitProcess(0)
    }
}