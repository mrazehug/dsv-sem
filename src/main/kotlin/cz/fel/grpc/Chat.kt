package cz.fel.grpc

import cz.fel.grpc.communication.Address
import org.apache.logging.log4j.core.config.ConfigurationFactory

fun main(args: Array<String>) {
    System.setProperty(ConfigurationFactory.CONFIGURATION_FILE_PROPERTY, "src/main/resources/log4j.properties");

    when(args.size) {
        2 -> {
            println("Starting node on ${args[0]}:${args[1]}")
            val address = Address(args[0], args[1].toInt())
            val node = Node(address)
            // node.run()
        }
        4 -> {
            val address = Address(args[0], args[1].toInt())
            val prevAddress = Address(args[2], args[3].toInt())
            val node = Node(address, prevAddress)
            node.run()
        }
    }
}