package cz.fel.grpc

import cz.fel.grpc.communication.Address
import cz.fel.grpc.utils.Logger
import java.util.regex.Matcher
import java.util.regex.Pattern

class CLI(val node: Node) : Runnable {
    val logger = Logger
    private fun handleHelp() {
        logger.cli("DSV-SEM v1.0.0 help")
        logger.cli("help - prints help")
        logger.cli("connections - prints connections")
        logger.cli("kill - kills node")
    }

    private fun printAddress(prefix: String, address: Address) {
        logger.cli("${prefix} - ${address.ipAddr}:${address.port}")
    }

    private fun printConnections() {
        printAddress("leader", node.leaderAddress)
        printAddress("p", node.pAddress);
        printAddress("me", node.address);
        printAddress("n", node.nAddress);
        printAddress("nn", node.nnAddress);
    }

    private fun printKeyRing() {
        logger.cli("Number of keys: ${node.nodeServiceClient.keyRing.size}")
        for (key in node.nodeServiceClient.keyRing) {
            logger.cli(key.key)
        }
    }

    private fun getText(text: String): String {
        var rv = ""
        var firstQuoteEncountered = false
        logger.cli(text)
        text.forEach {
            if(it == '"') {
                firstQuoteEncountered = true
            }

            if(firstQuoteEncountered && it != '"') {
                rv += it
            }
        }

        return rv
    }

    override fun run() {
        while(true) {
            print("> ")
            val line = readln()
            if(line != "") {
                print("> ")
                println(line)
            }
            else
                continue

            if(line.startsWith("show")) {
                val parts = line.split(" ");
                if(parts.size != 2) {
                    logger.cli("Invalid command")
                    continue
                }

                when(parts[1]) {
                    "connections" -> printConnections()
                    "keyring" -> printKeyRing()
                    "conf" -> printConfig()
                }
                continue
            }

            if(line.startsWith("send")) {
                // -ruid uid of recipient
                // -text "Text"
                val parts = mutableListOf<String>()

                val m: Matcher = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(line)
                while (m.find())
                    parts.add(m.group(1).replace("\"", ""));

                if(parts.size < 3) {
                    logger.cli("Incomplete command")
                    continue
                }

                var ruidIdx: Int?
                var ruid: String? = null
                var textIdx: Int?
                var text: String? = null

                try {
                    ruidIdx = parts.indexOf("-ruid")
                    ruid = parts[ruidIdx + 1]

                }
                catch (e: Exception) {
                    logger.cli("-ruid arg invalid")
                }

                try {
                    textIdx = parts.indexOf("-text")
                    text = parts[textIdx + 1]
                }
                catch (e: Exception) {
                    logger.cli("-text arg invalid")
                }

                if(ruid != null && text !== null) {
                    node.sendMessage(ruid, node.uid, text)
                }
                continue
            }

            if(line.startsWith("kill")) {
                node.kill()
                continue
            }

            if(line.startsWith("leave")) {
                node.leave()
                continue
            }

            if(line.startsWith("intercept")) {
                node.intercept = true
                continue
            }

            logger.cli("Unrecognized command: $line")
        }
    }

    private fun printConfig() {
        logger.cli("Hello, I am ${node.uid}")
        logger.cli("My address is ${node.address.ipAddr}:${node.address.port}")
    }
}