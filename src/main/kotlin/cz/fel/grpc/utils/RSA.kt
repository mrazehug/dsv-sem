package cz.fel.grpc.utils

// Important note: This code is taken from https://www.knowledgefactory.net/2021/01/kotlin-aes-rsa-3des-encryption-and.html
// Since there was not suitable library and it's not part of the core of the semwork
// I thought I will use it as a library.

import java.security.Key
import java.security.KeyFactory
import java.security.PrivateKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*
import javax.crypto.Cipher

object RSA {
    private fun loadPublicKey(stored: String): Key {
        val data: ByteArray = Base64.getDecoder().
        decode(stored.toByteArray())
        val spec = X509EncodedKeySpec(data)
        val fact = KeyFactory.getInstance("RSA")
        return fact.generatePublic(spec)
    }

    private fun loadPrivateKey(key64: String): PrivateKey {
        val clear: ByteArray = Base64.getDecoder().
        decode(key64.toByteArray())
        val keySpec = PKCS8EncodedKeySpec(clear)
        val fact = KeyFactory.getInstance("RSA")
        val priv = fact.generatePrivate(keySpec)
        Arrays.fill(clear, 0.toByte())
        return priv
    }

    fun encryptMessage(plainText: String, publickey: String): String {
        val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
        cipher.init(Cipher.ENCRYPT_MODE, loadPublicKey(publickey))
        return Base64.getEncoder().encodeToString(cipher.doFinal
            (plainText.toByteArray()))
    }

    fun decryptMessage(encryptedText: String?, privatekey: String):
            String {
        val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
        cipher.init(Cipher.DECRYPT_MODE, loadPrivateKey(privatekey))
        return String(cipher.doFinal(Base64.getDecoder().
        decode(encryptedText)))
    }
}