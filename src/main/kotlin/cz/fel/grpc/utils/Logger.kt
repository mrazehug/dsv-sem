package cz.fel.grpc.utils

import org.apache.logging.log4j.LogManager
import kotlinx.coroutines.sync.Mutex
import org.apache.logging.log4j.Level

object Logger {
    private val logger = LogManager.getLogger()

    fun info(time: Int, message: String) {
        logger.info("[${time}] $message")
        println("[INFO] [${time}] $message")
    }

    fun debug() {
        logger.debug("This is debug")
    }

    fun error(time: Int, message: String) {
        logger.error("[${time}] ${message}")
        println("[ERROR] [${time}] ${message}")
    }

    fun cli(message: String) {
        println("[CLI] $message")
    }
}