package cz.fel.grpc.communication

class Address(val ipAddr: String, val port: Int) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Address

        if (ipAddr != other.ipAddr) return false
        if (port != other.port) return false

        return true
    }

    override fun hashCode(): Int {
        var result = ipAddr.hashCode()
        result = 31 * result + port.hashCode()
        return result
    }
}