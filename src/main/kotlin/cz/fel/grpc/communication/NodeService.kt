package cz.fel.grpc.communication

import com.google.protobuf.Empty
import cz.fel.grpc.*
import cz.fel.grpc.Address
import cz.fel.grpc.utils.Logger
import cz.fel.grpc.utils.RSA
import io.grpc.Server
import io.grpc.ServerBuilder
import kotlin.math.log

class NodeService(private val node: Node)  : NodeServiceGrpcKt.NodeServiceCoroutineImplBase()  {
    private var isVoting = false
    private val logger = Logger

    val server: Server = ServerBuilder
        .forPort(node.address.port)
        .addService(this)
        .build()

    fun start() {
        server.start()
        println("Server started, listening on ${node.address.port}")
        Runtime.getRuntime().addShutdownHook(
            Thread {
                println("*** shutting down gRPC server since JVM is shutting down")
                this@NodeService.stop()
                println("*** server shut down")
            }
        )
    }

    private fun stop() {
        server.shutdown()
    }

    fun blockUntilShutdown() {
        server.awaitTermination()
    }

    override suspend fun join(request: JoinRequest): JoinResponse {
        node.time.set(maxOf(node.time.get(), request.time) + 1)
        logger.info(node.time.get(), "Join requested by node ${request.address.ipAddr}:${request.address.port}")

        val pAddress = Address.newBuilder().setIpAddr(node.pAddress.ipAddr).setPort(node.pAddress.port).build()
        val nAddress = Address.newBuilder().setIpAddr(node.nAddress.ipAddr).setPort(node.nAddress.port).build()
        val nnAddress: Address?;
        val leaderAddress = Address.newBuilder().setIpAddr(node.leaderAddress.ipAddr).setPort(node.leaderAddress.port).build()

        if(node.pAddress != node.address) {
           nnAddress = Address.newBuilder().setIpAddr(node.nnAddress.ipAddr).setPort(node.nnAddress.port).build()
           node.nodeServiceClient.updateNNNode(node.pAddress, Address(request.address.ipAddr, request.address.port))
        }
        else {
            nnAddress = request.address
        }

        node.nodeServiceClient.updatePNode(node.nAddress, Address(request.address.ipAddr, request.address.port))
        node.nnAddress = node.nAddress;

        node.nAddress = Address(request.address.ipAddr, request.address.port)

        return JoinResponse.newBuilder()
            .setPAddress(pAddress)
            .setNAddress(nAddress)
            .setNnAddress(nnAddress)
            .setLeaderAddress(leaderAddress)
            .setTime(node.time.incrementAndGet())
            .build();
    }

    override suspend fun election(request: ElectionRequest): Empty {
        node.time.set(maxOf(node.time.get(), request.time) + 1)
        logger.info(node.time.get(), "Received election request.")

        if(request.senderUID > node.uid) {
            isVoting = true
            logger.info(node.time.get(), "Received higher UID. Passing it on")
            node.nodeServiceClient.election(request.senderUID)

            return Empty.newBuilder().build();
        }

        if(request.senderUID < node.uid && !isVoting) {
            isVoting = true
            logger.info(node.time.get(), "I have higher UID. Overriding vote.")
            node.nodeServiceClient.election(node.uid)

            return Empty.newBuilder().build();
        }

        if(request.senderUID == node.uid) {
            logger.info(node.time.get(), "No one had higher UID, I am becoming the leader.")
            node.nodeServiceClient.elected(node.uid, node.address)
        }

        return Empty.newBuilder().build();
    }

    override suspend fun elected(request: ElectedRequest): Empty {
        node.time.set(maxOf(node.time.get(), request.time) + 1)
        logger.info(node.time.get(), "Received elected request. New leader is ${request.leaderAddress.ipAddr}:${request.leaderAddress.port}")
        node.leaderAddress = Address(request.leaderAddress.ipAddr, request.leaderAddress.port)

        if(request.leaderUID != node.uid)
            node.nodeServiceClient.elected(request.leaderUID, node.leaderAddress)

        return Empty.newBuilder().build()
    }

    override suspend fun updateNNNode(request: UpdateNNNodeRequest): Empty {
        node.time.set(maxOf(node.time.get(), request.time) + 1)
        logger.info(node.time.get(), "Received updatePNode request. My new nnNode is ${request.address.ipAddr}:${request.address.port}")

        node.nnAddress = Address(request.address.ipAddr, request.address.port)
        return Empty.newBuilder().build();
    }

    override suspend fun updatePNode(request: UpdatePNodeRequest): UpdatePNodeResponse {
        node.time.set(maxOf(node.time.get(), request.time) + 1)
        logger.info(node.time.get(), "Received updatePNode request. My new pNode is ${request.address.ipAddr}:${request.address.port}")
        node.pAddress = Address(request.address.ipAddr, request.address.port)
        return UpdatePNodeResponse.newBuilder().setAddress(
            Address.newBuilder().setIpAddr(node.nAddress.ipAddr).setPort(node.nAddress.port)
        ).setTime(node.time.incrementAndGet())
            .build()
    }

    override suspend fun sendMessage(request: SendMessageRequest): Empty {
        node.time.set(maxOf(node.time.get(), request.time) + 1)
        logger.info(node.time.get(), "Received message.")

        if(request.senderUID == node.uid && request.recipientUID != node.uid) {
            logger.info(node.time.get(), "This message was sent by me.")
        } else if(request.recipientUID == node.uid) {
            logger.info(node.time.get(), "This message is for me! It says ${RSA.decryptMessage(request.text, node.privateKey)}")
        } else if(node.intercept) {
            logger.info(node.time.get(), "This message is not for me, but I was told to intercept the messages, trying to read it.")
            logger.info(node.time.get(), "The message says ${request.text}")
            logger.info(node.time.get(), "Oh no, without the key it's just gibberish! What a shame. Passing on!")
            node.nodeServiceClient.sendMessage(request.recipientUID, request.senderUID, request.text)
        } else {
            logger.info(node.time.get(), "This message is not for me. Passing on!")
            node.nodeServiceClient.sendMessage(request.recipientUID, request.senderUID, request.text)
        }

        return Empty.newBuilder().build()
    }

    override suspend fun publishPublicKey(request: PublishPublicKeyRequest): Empty {
        node.time.set(maxOf(node.time.get(), request.time) + 1)
        logger.info(node.time.get(), "Received public key publish request of ${request.senderUID}.")
        if(request.senderUID == node.uid) {
            logger.info(node.time.get(), "I sent this request. Adding all other keys to keychain.")
            for (keychainEntry in request.keychainList) {
                node.nodeServiceClient.addKey(keychainEntry.senderUID, keychainEntry.publicKey)
            }
        } else {
            logger.info(node.time.get(), "I haven't seen this key yet. Adding to keychain and adding my key to request.")
            node.nodeServiceClient.addKey(request.senderUID, request.publicKey)
            node.nodeServiceClient.publishPublicKey(request.senderUID, request.publicKey, request.keychainList)
        }

        return Empty.newBuilder().build()
    }

    override suspend fun missingNode(request: MissingNodeRequest): Empty {
        node.time.set(maxOf(node.time.get(), request.time) + 1)
        logger.info(node.time.get(), "Received missing node request. Missing node is ${request.address.ipAddr}:${request.address.port}")

        val missingAddress = Address(request.address.ipAddr, request.address.port);
        if(missingAddress != node.nAddress) {
            node.nodeServiceClient.missingNode(missingAddress)
        }
        else {
            node.nAddress = node.nnAddress
            val newNNAddress = node.nodeServiceClient.updatePNode(node.nnAddress, node.address).address
            node.nnAddress = Address(newNNAddress.ipAddr, newNNAddress.port)
            node.nodeServiceClient.updateNNNode(node.pAddress, node.nAddress)
        }

        return Empty.newBuilder().build();
    }

    override suspend fun leave(request: LeaveRequest): Empty {
        val missingNodeRequest = MissingNodeRequest.newBuilder()
            .setTime(request.time)
            .setAddress(
                Address.newBuilder().setIpAddr(request.address.ipAddr).setPort(request.address.port)
            )
            .build()

        this.missingNode(missingNodeRequest)

        if(Address(request.address.ipAddr, request.address.port) == node.leaderAddress)
            this.node.nodeServiceClient.election(node.uid)

        return Empty.newBuilder().build()
    }
}