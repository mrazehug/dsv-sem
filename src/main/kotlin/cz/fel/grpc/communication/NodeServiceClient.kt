package cz.fel.grpc.communication

import cz.fel.grpc.*
import cz.fel.grpc.utils.Logger
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.StatusException
import java.util.concurrent.atomic.AtomicInteger


class NodeServiceClient(val node: Node) : INodeServiceClient  {

    // Stores channels to other nodes
    val channels : MutableMap<String, ManagedChannel> = mutableMapOf()

    // Stores public keys of other nodes so you can encrypt messages
    val keyRing: MutableMap<String, String> = mutableMapOf()

    private var voting = false;
    private var repairing: Boolean = false;
    private val logger = Logger

    private fun constructChannelForAddress(address: Address) : ManagedChannel {
        val key = address.ipAddr + ":" + address.port

        if(channels[key] == null) {
            val channel =  ManagedChannelBuilder.forAddress(address.ipAddr, address.port)
                .usePlaintext()
                .build()
            channels[key] = channel;
        }

        return channels[key] as ManagedChannel
    }

    fun addKey(uid: String, key: String) {
        keyRing[uid] = key;
    }

    override suspend fun join() {
        logger.info(node.time.incrementAndGet(), "Node ${node.address.ipAddr}:${node.address.port} requests joining")

        if(node.address == node.pAddress) {
            node.leaderAddress = node.address
            logger.info(node.time.incrementAndGet(), "I am first, therefore elected as leader")
            return
        }

        val joinRequest = JoinRequest.newBuilder()
            .setAddress(
                cz.fel.grpc.Address.newBuilder()
                    .setIpAddr(node.address.ipAddr)
                    .setPort(node.address.port)
                .build()
            )
            .setTime(node.time.get())
            .build()

        NodeServiceGrpcKt.NodeServiceCoroutineStub(constructChannelForAddress(node.pAddress))
            .join(joinRequest)
            .apply {
                node.time.set(maxOf(node.time.get(), time) + 1)
                logger.info(node.time.get(), "Join accepted by prev node ${node.pAddress.ipAddr}:${node.pAddress.port}")
                node.nAddress = Address(nAddress.ipAddr, nAddress.port)
                node.nnAddress = Address(nnAddress.ipAddr, nnAddress.port)
                node.leaderAddress = Address(leaderAddress.ipAddr, leaderAddress.port)
            }
    }

    override suspend fun updatePNode(target: Address, address: Address): UpdatePNodeResponse {
        logger.info(node.time.incrementAndGet(), "Updating PNode. Target is ${target.ipAddr}:${target.port}, his new NNAddress is ${address.ipAddr}:${address.port}")
        val updatePNodeRequest = UpdatePNodeRequest.newBuilder()
            .setAddress(
                cz.fel.grpc.Address.newBuilder().setIpAddr(address.ipAddr).setPort(address.port)
            ).build();

        return NodeServiceGrpcKt.NodeServiceCoroutineStub(constructChannelForAddress(target))
            .updatePNode(updatePNodeRequest)
    }

    override suspend fun updateNNNode(target: Address, address: Address) {
        logger.info(node.time.incrementAndGet(), "Updating NNNode. Target is ${target.ipAddr}:${target.port}, his new NNAddress is ${address.ipAddr}:${address.port}")
        val updateNNNodeRequest = UpdateNNNodeRequest.newBuilder()
            .setAddress(
                cz.fel.grpc.Address.newBuilder().setIpAddr(address.ipAddr).setPort(address.port)
            ).build();

        NodeServiceGrpcKt.NodeServiceCoroutineStub(constructChannelForAddress(target))
            .updateNNNode(updateNNNodeRequest)
    }

    override suspend fun election(candidateUID: String) {
        logger.info(node.time.incrementAndGet(), "Sending election. Candidate is ${candidateUID}${if (candidateUID == node.uid) " (me)" else ""}")
        val electionRequest = ElectionRequest.newBuilder().setSenderUID(candidateUID).build();

        NodeServiceGrpcKt.NodeServiceCoroutineStub(constructChannelForAddress(node.nAddress))
            .election(electionRequest)
    }

    override suspend fun elected(leaderUID: String, leaderAddress: Address) {
        logger.info(node.time.incrementAndGet(), "Sharing elected info. Leader is ${leaderAddress.ipAddr}:${leaderAddress.port}")
        val electedRequest = ElectedRequest.newBuilder()
            .setLeaderAddress(
            cz.fel.grpc.Address.newBuilder().setIpAddr(leaderAddress.ipAddr).setPort(leaderAddress.port)
        )
            .setLeaderUID(leaderUID)
            .build()

        NodeServiceGrpcKt.NodeServiceCoroutineStub(constructChannelForAddress(node.nAddress))
            .elected(electedRequest);
    }

    override suspend fun sendMessage(recipientUID: String, senderUID: String, text: String) {
        val sendMessageRequest = SendMessageRequest.newBuilder()
            .setRecipientUID(recipientUID)
            .setSenderUID(senderUID)
            .setText(text)
            .build()

        try {
            logger.info(node.time.incrementAndGet(), "Sending message to ${recipientUID}")
            NodeServiceGrpcKt.NodeServiceCoroutineStub(constructChannelForAddress(node.nAddress))
                .sendMessage(sendMessageRequest)
        }
        catch (e: StatusException) {
            logger.error(node.time.incrementAndGet(), "Detected missing node. Attempting repair.")
            repairTopology(node.nAddress);
        }
    }

    private suspend fun repairTopology(missingNodeAddress: Address) {
        if(repairing) {
            return
        }

        repairing = true;

        val missingNodeRequest = MissingNodeRequest.newBuilder()
            .setTime(node.time.get())
            .setAddress(
                cz.fel.grpc.Address.newBuilder()
                    .setIpAddr(missingNodeAddress.ipAddr)
                    .setPort(missingNodeAddress.port)
                    .build()
            ).build()

        node.nodeService.missingNode(missingNodeRequest)

        repairing = false

        if(missingNodeAddress == node.leaderAddress) {
            election(node.uid)
        }
    }

    override suspend fun missingNode(missingNodeAddress: Address) {
        logger.info(node.time.incrementAndGet(), "Reporting missing node - ${missingNodeAddress.ipAddr}:${missingNodeAddress.port} to my next")

        val missingNodeRequest = MissingNodeRequest.newBuilder()
            .setAddress(
                cz.fel.grpc.Address.newBuilder()
                    .setIpAddr(missingNodeAddress.ipAddr)
                    .setPort(missingNodeAddress.port)
                    .build()
            ).build()

        NodeServiceGrpcKt.NodeServiceCoroutineStub(constructChannelForAddress(node.nAddress))
            .missingNode(missingNodeRequest)
    }

    override suspend fun publishPublicKey(senderUID: String, key: String, keychainList: MutableList<KeychainEntry>) {
        logger.info(node.time.incrementAndGet(), "Sharing public key shared by ${senderUID}")
        val publishPublicKeyRequest = PublishPublicKeyRequest.newBuilder()
            .setSenderUID(senderUID)
            .setPublicKey(key)
            .addAllKeychain(keychainList)
            .addKeychain(
                KeychainEntry.newBuilder().setPublicKey(node.publicKey).setSenderUID(node.uid)
            )
            .build()

        NodeServiceGrpcKt.NodeServiceCoroutineStub(constructChannelForAddress(node.nAddress))
            .publishPublicKey(publishPublicKeyRequest)
    }

    override suspend fun leave(leavingAddress: Address) {
        logger.info(node.time.incrementAndGet(), "Leaving and notifying next")

        val leaveRequest = LeaveRequest.newBuilder()
            .setAddress(
                cz.fel.grpc.Address.newBuilder().setIpAddr(leavingAddress.ipAddr).setPort(leavingAddress.port)
            )
            .build()

        NodeServiceGrpcKt.NodeServiceCoroutineStub(constructChannelForAddress(node.nAddress))
            .leave(leaveRequest)
    }
}