package cz.fel.grpc.communication

import cz.fel.grpc.KeychainEntry
import cz.fel.grpc.UpdatePNodeResponse

interface INodeServiceClient {
    suspend fun join()
    suspend fun updatePNode(target: Address, address: Address): UpdatePNodeResponse
    suspend fun updateNNNode(target: Address, address: Address)
    suspend fun missingNode(missingNodeAddress: Address)
    suspend fun leave(leavingAddress: Address)
    suspend fun election(candidateUID: String)
    suspend fun elected(leaderUID: String, leaderAddress: Address)
    suspend fun sendMessage(recipientUID: String, senderUID: String, text: String)
    suspend fun publishPublicKey(senderUID: String, key: String, keychainList: MutableList<KeychainEntry>)
}