package cz.fel.grpc

import com.google.protobuf.Empty
import cz.fel.grpc.NodeServiceGrpc.getServiceDescriptor
import io.grpc.CallOptions
import io.grpc.CallOptions.DEFAULT
import io.grpc.Channel
import io.grpc.Metadata
import io.grpc.MethodDescriptor
import io.grpc.ServerServiceDefinition
import io.grpc.ServerServiceDefinition.builder
import io.grpc.ServiceDescriptor
import io.grpc.Status
import io.grpc.Status.UNIMPLEMENTED
import io.grpc.StatusException
import io.grpc.kotlin.AbstractCoroutineServerImpl
import io.grpc.kotlin.AbstractCoroutineStub
import io.grpc.kotlin.ClientCalls
import io.grpc.kotlin.ClientCalls.unaryRpc
import io.grpc.kotlin.ServerCalls
import io.grpc.kotlin.ServerCalls.unaryServerMethodDefinition
import io.grpc.kotlin.StubFor
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.jvm.JvmOverloads
import kotlin.jvm.JvmStatic

/**
 * Holder for Kotlin coroutine-based client and server APIs for cz.fel.grpc.NodeService.
 */
object NodeServiceGrpcKt {
  @JvmStatic
  val serviceDescriptor: ServiceDescriptor
    get() = NodeServiceGrpc.getServiceDescriptor()

  val joinMethod: MethodDescriptor<JoinRequest, JoinResponse>
    @JvmStatic
    get() = NodeServiceGrpc.getJoinMethod()

  val updatePNodeMethod: MethodDescriptor<UpdatePNodeRequest, UpdatePNodeResponse>
    @JvmStatic
    get() = NodeServiceGrpc.getUpdatePNodeMethod()

  val updateNNNodeMethod: MethodDescriptor<UpdateNNNodeRequest, Empty>
    @JvmStatic
    get() = NodeServiceGrpc.getUpdateNNNodeMethod()

  val missingNodeMethod: MethodDescriptor<MissingNodeRequest, Empty>
    @JvmStatic
    get() = NodeServiceGrpc.getMissingNodeMethod()

  val leaveMethod: MethodDescriptor<LeaveRequest, Empty>
    @JvmStatic
    get() = NodeServiceGrpc.getLeaveMethod()

  val electionMethod: MethodDescriptor<ElectionRequest, Empty>
    @JvmStatic
    get() = NodeServiceGrpc.getElectionMethod()

  val electedMethod: MethodDescriptor<ElectedRequest, Empty>
    @JvmStatic
    get() = NodeServiceGrpc.getElectedMethod()

  val publishPublicKeyMethod: MethodDescriptor<PublishPublicKeyRequest, Empty>
    @JvmStatic
    get() = NodeServiceGrpc.getPublishPublicKeyMethod()

  val sendMessageMethod: MethodDescriptor<SendMessageRequest, Empty>
    @JvmStatic
    get() = NodeServiceGrpc.getSendMessageMethod()

  /**
   * A stub for issuing RPCs to a(n) cz.fel.grpc.NodeService service as suspending coroutines.
   */
  @StubFor(NodeServiceGrpc::class)
  class NodeServiceCoroutineStub @JvmOverloads constructor(
    channel: Channel,
    callOptions: CallOptions = DEFAULT
  ) : AbstractCoroutineStub<NodeServiceCoroutineStub>(channel, callOptions) {
    override fun build(channel: Channel, callOptions: CallOptions): NodeServiceCoroutineStub =
        NodeServiceCoroutineStub(channel, callOptions)

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @return The single response from the server.
     */
    suspend fun join(request: JoinRequest): JoinResponse = unaryRpc(
      channel,
      NodeServiceGrpc.getJoinMethod(),
      request,
      callOptions,
      Metadata()
    )
    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @return The single response from the server.
     */
    suspend fun updatePNode(request: UpdatePNodeRequest): UpdatePNodeResponse = unaryRpc(
      channel,
      NodeServiceGrpc.getUpdatePNodeMethod(),
      request,
      callOptions,
      Metadata()
    )
    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @return The single response from the server.
     */
    suspend fun updateNNNode(request: UpdateNNNodeRequest): Empty = unaryRpc(
      channel,
      NodeServiceGrpc.getUpdateNNNodeMethod(),
      request,
      callOptions,
      Metadata()
    )
    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @return The single response from the server.
     */
    suspend fun missingNode(request: MissingNodeRequest): Empty = unaryRpc(
      channel,
      NodeServiceGrpc.getMissingNodeMethod(),
      request,
      callOptions,
      Metadata()
    )
    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @return The single response from the server.
     */
    suspend fun leave(request: LeaveRequest): Empty = unaryRpc(
      channel,
      NodeServiceGrpc.getLeaveMethod(),
      request,
      callOptions,
      Metadata()
    )
    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @return The single response from the server.
     */
    suspend fun election(request: ElectionRequest): Empty = unaryRpc(
      channel,
      NodeServiceGrpc.getElectionMethod(),
      request,
      callOptions,
      Metadata()
    )
    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @return The single response from the server.
     */
    suspend fun elected(request: ElectedRequest): Empty = unaryRpc(
      channel,
      NodeServiceGrpc.getElectedMethod(),
      request,
      callOptions,
      Metadata()
    )
    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @return The single response from the server.
     */
    suspend fun publishPublicKey(request: PublishPublicKeyRequest): Empty = unaryRpc(
      channel,
      NodeServiceGrpc.getPublishPublicKeyMethod(),
      request,
      callOptions,
      Metadata()
    )
    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @return The single response from the server.
     */
    suspend fun sendMessage(request: SendMessageRequest): Empty = unaryRpc(
      channel,
      NodeServiceGrpc.getSendMessageMethod(),
      request,
      callOptions,
      Metadata()
    )}

  /**
   * Skeletal implementation of the cz.fel.grpc.NodeService service based on Kotlin coroutines.
   */
  abstract class NodeServiceCoroutineImplBase(
    coroutineContext: CoroutineContext = EmptyCoroutineContext
  ) : AbstractCoroutineServerImpl(coroutineContext) {
    /**
     * Returns the response to an RPC for cz.fel.grpc.NodeService.Join.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun join(request: JoinRequest): JoinResponse = throw
        StatusException(UNIMPLEMENTED.withDescription("Method cz.fel.grpc.NodeService.Join is unimplemented"))

    /**
     * Returns the response to an RPC for cz.fel.grpc.NodeService.UpdatePNode.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun updatePNode(request: UpdatePNodeRequest): UpdatePNodeResponse = throw
        StatusException(UNIMPLEMENTED.withDescription("Method cz.fel.grpc.NodeService.UpdatePNode is unimplemented"))

    /**
     * Returns the response to an RPC for cz.fel.grpc.NodeService.UpdateNNNode.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun updateNNNode(request: UpdateNNNodeRequest): Empty = throw
        StatusException(UNIMPLEMENTED.withDescription("Method cz.fel.grpc.NodeService.UpdateNNNode is unimplemented"))

    /**
     * Returns the response to an RPC for cz.fel.grpc.NodeService.MissingNode.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun missingNode(request: MissingNodeRequest): Empty = throw
        StatusException(UNIMPLEMENTED.withDescription("Method cz.fel.grpc.NodeService.MissingNode is unimplemented"))

    /**
     * Returns the response to an RPC for cz.fel.grpc.NodeService.Leave.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun leave(request: LeaveRequest): Empty = throw
        StatusException(UNIMPLEMENTED.withDescription("Method cz.fel.grpc.NodeService.Leave is unimplemented"))

    /**
     * Returns the response to an RPC for cz.fel.grpc.NodeService.Election.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun election(request: ElectionRequest): Empty = throw
        StatusException(UNIMPLEMENTED.withDescription("Method cz.fel.grpc.NodeService.Election is unimplemented"))

    /**
     * Returns the response to an RPC for cz.fel.grpc.NodeService.Elected.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun elected(request: ElectedRequest): Empty = throw
        StatusException(UNIMPLEMENTED.withDescription("Method cz.fel.grpc.NodeService.Elected is unimplemented"))

    /**
     * Returns the response to an RPC for cz.fel.grpc.NodeService.PublishPublicKey.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun publishPublicKey(request: PublishPublicKeyRequest): Empty = throw
        StatusException(UNIMPLEMENTED.withDescription("Method cz.fel.grpc.NodeService.PublishPublicKey is unimplemented"))

    /**
     * Returns the response to an RPC for cz.fel.grpc.NodeService.SendMessage.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun sendMessage(request: SendMessageRequest): Empty = throw
        StatusException(UNIMPLEMENTED.withDescription("Method cz.fel.grpc.NodeService.SendMessage is unimplemented"))

    final override fun bindService(): ServerServiceDefinition = builder(getServiceDescriptor())
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = NodeServiceGrpc.getJoinMethod(),
      implementation = ::join
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = NodeServiceGrpc.getUpdatePNodeMethod(),
      implementation = ::updatePNode
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = NodeServiceGrpc.getUpdateNNNodeMethod(),
      implementation = ::updateNNNode
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = NodeServiceGrpc.getMissingNodeMethod(),
      implementation = ::missingNode
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = NodeServiceGrpc.getLeaveMethod(),
      implementation = ::leave
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = NodeServiceGrpc.getElectionMethod(),
      implementation = ::election
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = NodeServiceGrpc.getElectedMethod(),
      implementation = ::elected
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = NodeServiceGrpc.getPublishPublicKeyMethod(),
      implementation = ::publishPublicKey
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = NodeServiceGrpc.getSendMessageMethod(),
      implementation = ::sendMessage
    )).build()
  }
}
