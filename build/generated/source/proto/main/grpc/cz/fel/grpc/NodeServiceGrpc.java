package cz.fel.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.33.1)",
    comments = "Source: NodeService.proto")
public final class NodeServiceGrpc {

  private NodeServiceGrpc() {}

  public static final String SERVICE_NAME = "cz.fel.grpc.NodeService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<cz.fel.grpc.JoinRequest,
      cz.fel.grpc.JoinResponse> getJoinMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Join",
      requestType = cz.fel.grpc.JoinRequest.class,
      responseType = cz.fel.grpc.JoinResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<cz.fel.grpc.JoinRequest,
      cz.fel.grpc.JoinResponse> getJoinMethod() {
    io.grpc.MethodDescriptor<cz.fel.grpc.JoinRequest, cz.fel.grpc.JoinResponse> getJoinMethod;
    if ((getJoinMethod = NodeServiceGrpc.getJoinMethod) == null) {
      synchronized (NodeServiceGrpc.class) {
        if ((getJoinMethod = NodeServiceGrpc.getJoinMethod) == null) {
          NodeServiceGrpc.getJoinMethod = getJoinMethod =
              io.grpc.MethodDescriptor.<cz.fel.grpc.JoinRequest, cz.fel.grpc.JoinResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Join"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.JoinRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.JoinResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NodeServiceMethodDescriptorSupplier("Join"))
              .build();
        }
      }
    }
    return getJoinMethod;
  }

  private static volatile io.grpc.MethodDescriptor<cz.fel.grpc.UpdatePNodeRequest,
      cz.fel.grpc.UpdatePNodeResponse> getUpdatePNodeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "UpdatePNode",
      requestType = cz.fel.grpc.UpdatePNodeRequest.class,
      responseType = cz.fel.grpc.UpdatePNodeResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<cz.fel.grpc.UpdatePNodeRequest,
      cz.fel.grpc.UpdatePNodeResponse> getUpdatePNodeMethod() {
    io.grpc.MethodDescriptor<cz.fel.grpc.UpdatePNodeRequest, cz.fel.grpc.UpdatePNodeResponse> getUpdatePNodeMethod;
    if ((getUpdatePNodeMethod = NodeServiceGrpc.getUpdatePNodeMethod) == null) {
      synchronized (NodeServiceGrpc.class) {
        if ((getUpdatePNodeMethod = NodeServiceGrpc.getUpdatePNodeMethod) == null) {
          NodeServiceGrpc.getUpdatePNodeMethod = getUpdatePNodeMethod =
              io.grpc.MethodDescriptor.<cz.fel.grpc.UpdatePNodeRequest, cz.fel.grpc.UpdatePNodeResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "UpdatePNode"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.UpdatePNodeRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.UpdatePNodeResponse.getDefaultInstance()))
              .setSchemaDescriptor(new NodeServiceMethodDescriptorSupplier("UpdatePNode"))
              .build();
        }
      }
    }
    return getUpdatePNodeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<cz.fel.grpc.UpdateNNNodeRequest,
      com.google.protobuf.Empty> getUpdateNNNodeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "UpdateNNNode",
      requestType = cz.fel.grpc.UpdateNNNodeRequest.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<cz.fel.grpc.UpdateNNNodeRequest,
      com.google.protobuf.Empty> getUpdateNNNodeMethod() {
    io.grpc.MethodDescriptor<cz.fel.grpc.UpdateNNNodeRequest, com.google.protobuf.Empty> getUpdateNNNodeMethod;
    if ((getUpdateNNNodeMethod = NodeServiceGrpc.getUpdateNNNodeMethod) == null) {
      synchronized (NodeServiceGrpc.class) {
        if ((getUpdateNNNodeMethod = NodeServiceGrpc.getUpdateNNNodeMethod) == null) {
          NodeServiceGrpc.getUpdateNNNodeMethod = getUpdateNNNodeMethod =
              io.grpc.MethodDescriptor.<cz.fel.grpc.UpdateNNNodeRequest, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "UpdateNNNode"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.UpdateNNNodeRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new NodeServiceMethodDescriptorSupplier("UpdateNNNode"))
              .build();
        }
      }
    }
    return getUpdateNNNodeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<cz.fel.grpc.MissingNodeRequest,
      com.google.protobuf.Empty> getMissingNodeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "MissingNode",
      requestType = cz.fel.grpc.MissingNodeRequest.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<cz.fel.grpc.MissingNodeRequest,
      com.google.protobuf.Empty> getMissingNodeMethod() {
    io.grpc.MethodDescriptor<cz.fel.grpc.MissingNodeRequest, com.google.protobuf.Empty> getMissingNodeMethod;
    if ((getMissingNodeMethod = NodeServiceGrpc.getMissingNodeMethod) == null) {
      synchronized (NodeServiceGrpc.class) {
        if ((getMissingNodeMethod = NodeServiceGrpc.getMissingNodeMethod) == null) {
          NodeServiceGrpc.getMissingNodeMethod = getMissingNodeMethod =
              io.grpc.MethodDescriptor.<cz.fel.grpc.MissingNodeRequest, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "MissingNode"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.MissingNodeRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new NodeServiceMethodDescriptorSupplier("MissingNode"))
              .build();
        }
      }
    }
    return getMissingNodeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<cz.fel.grpc.LeaveRequest,
      com.google.protobuf.Empty> getLeaveMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Leave",
      requestType = cz.fel.grpc.LeaveRequest.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<cz.fel.grpc.LeaveRequest,
      com.google.protobuf.Empty> getLeaveMethod() {
    io.grpc.MethodDescriptor<cz.fel.grpc.LeaveRequest, com.google.protobuf.Empty> getLeaveMethod;
    if ((getLeaveMethod = NodeServiceGrpc.getLeaveMethod) == null) {
      synchronized (NodeServiceGrpc.class) {
        if ((getLeaveMethod = NodeServiceGrpc.getLeaveMethod) == null) {
          NodeServiceGrpc.getLeaveMethod = getLeaveMethod =
              io.grpc.MethodDescriptor.<cz.fel.grpc.LeaveRequest, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Leave"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.LeaveRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new NodeServiceMethodDescriptorSupplier("Leave"))
              .build();
        }
      }
    }
    return getLeaveMethod;
  }

  private static volatile io.grpc.MethodDescriptor<cz.fel.grpc.ElectionRequest,
      com.google.protobuf.Empty> getElectionMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Election",
      requestType = cz.fel.grpc.ElectionRequest.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<cz.fel.grpc.ElectionRequest,
      com.google.protobuf.Empty> getElectionMethod() {
    io.grpc.MethodDescriptor<cz.fel.grpc.ElectionRequest, com.google.protobuf.Empty> getElectionMethod;
    if ((getElectionMethod = NodeServiceGrpc.getElectionMethod) == null) {
      synchronized (NodeServiceGrpc.class) {
        if ((getElectionMethod = NodeServiceGrpc.getElectionMethod) == null) {
          NodeServiceGrpc.getElectionMethod = getElectionMethod =
              io.grpc.MethodDescriptor.<cz.fel.grpc.ElectionRequest, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Election"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.ElectionRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new NodeServiceMethodDescriptorSupplier("Election"))
              .build();
        }
      }
    }
    return getElectionMethod;
  }

  private static volatile io.grpc.MethodDescriptor<cz.fel.grpc.ElectedRequest,
      com.google.protobuf.Empty> getElectedMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Elected",
      requestType = cz.fel.grpc.ElectedRequest.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<cz.fel.grpc.ElectedRequest,
      com.google.protobuf.Empty> getElectedMethod() {
    io.grpc.MethodDescriptor<cz.fel.grpc.ElectedRequest, com.google.protobuf.Empty> getElectedMethod;
    if ((getElectedMethod = NodeServiceGrpc.getElectedMethod) == null) {
      synchronized (NodeServiceGrpc.class) {
        if ((getElectedMethod = NodeServiceGrpc.getElectedMethod) == null) {
          NodeServiceGrpc.getElectedMethod = getElectedMethod =
              io.grpc.MethodDescriptor.<cz.fel.grpc.ElectedRequest, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Elected"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.ElectedRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new NodeServiceMethodDescriptorSupplier("Elected"))
              .build();
        }
      }
    }
    return getElectedMethod;
  }

  private static volatile io.grpc.MethodDescriptor<cz.fel.grpc.PublishPublicKeyRequest,
      com.google.protobuf.Empty> getPublishPublicKeyMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "PublishPublicKey",
      requestType = cz.fel.grpc.PublishPublicKeyRequest.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<cz.fel.grpc.PublishPublicKeyRequest,
      com.google.protobuf.Empty> getPublishPublicKeyMethod() {
    io.grpc.MethodDescriptor<cz.fel.grpc.PublishPublicKeyRequest, com.google.protobuf.Empty> getPublishPublicKeyMethod;
    if ((getPublishPublicKeyMethod = NodeServiceGrpc.getPublishPublicKeyMethod) == null) {
      synchronized (NodeServiceGrpc.class) {
        if ((getPublishPublicKeyMethod = NodeServiceGrpc.getPublishPublicKeyMethod) == null) {
          NodeServiceGrpc.getPublishPublicKeyMethod = getPublishPublicKeyMethod =
              io.grpc.MethodDescriptor.<cz.fel.grpc.PublishPublicKeyRequest, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "PublishPublicKey"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.PublishPublicKeyRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new NodeServiceMethodDescriptorSupplier("PublishPublicKey"))
              .build();
        }
      }
    }
    return getPublishPublicKeyMethod;
  }

  private static volatile io.grpc.MethodDescriptor<cz.fel.grpc.SendMessageRequest,
      com.google.protobuf.Empty> getSendMessageMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SendMessage",
      requestType = cz.fel.grpc.SendMessageRequest.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<cz.fel.grpc.SendMessageRequest,
      com.google.protobuf.Empty> getSendMessageMethod() {
    io.grpc.MethodDescriptor<cz.fel.grpc.SendMessageRequest, com.google.protobuf.Empty> getSendMessageMethod;
    if ((getSendMessageMethod = NodeServiceGrpc.getSendMessageMethod) == null) {
      synchronized (NodeServiceGrpc.class) {
        if ((getSendMessageMethod = NodeServiceGrpc.getSendMessageMethod) == null) {
          NodeServiceGrpc.getSendMessageMethod = getSendMessageMethod =
              io.grpc.MethodDescriptor.<cz.fel.grpc.SendMessageRequest, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SendMessage"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  cz.fel.grpc.SendMessageRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new NodeServiceMethodDescriptorSupplier("SendMessage"))
              .build();
        }
      }
    }
    return getSendMessageMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static NodeServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<NodeServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<NodeServiceStub>() {
        @java.lang.Override
        public NodeServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new NodeServiceStub(channel, callOptions);
        }
      };
    return NodeServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static NodeServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<NodeServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<NodeServiceBlockingStub>() {
        @java.lang.Override
        public NodeServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new NodeServiceBlockingStub(channel, callOptions);
        }
      };
    return NodeServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static NodeServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<NodeServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<NodeServiceFutureStub>() {
        @java.lang.Override
        public NodeServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new NodeServiceFutureStub(channel, callOptions);
        }
      };
    return NodeServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class NodeServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void join(cz.fel.grpc.JoinRequest request,
        io.grpc.stub.StreamObserver<cz.fel.grpc.JoinResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getJoinMethod(), responseObserver);
    }

    /**
     */
    public void updatePNode(cz.fel.grpc.UpdatePNodeRequest request,
        io.grpc.stub.StreamObserver<cz.fel.grpc.UpdatePNodeResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdatePNodeMethod(), responseObserver);
    }

    /**
     */
    public void updateNNNode(cz.fel.grpc.UpdateNNNodeRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateNNNodeMethod(), responseObserver);
    }

    /**
     */
    public void missingNode(cz.fel.grpc.MissingNodeRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getMissingNodeMethod(), responseObserver);
    }

    /**
     */
    public void leave(cz.fel.grpc.LeaveRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getLeaveMethod(), responseObserver);
    }

    /**
     */
    public void election(cz.fel.grpc.ElectionRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getElectionMethod(), responseObserver);
    }

    /**
     */
    public void elected(cz.fel.grpc.ElectedRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getElectedMethod(), responseObserver);
    }

    /**
     */
    public void publishPublicKey(cz.fel.grpc.PublishPublicKeyRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getPublishPublicKeyMethod(), responseObserver);
    }

    /**
     */
    public void sendMessage(cz.fel.grpc.SendMessageRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getSendMessageMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getJoinMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                cz.fel.grpc.JoinRequest,
                cz.fel.grpc.JoinResponse>(
                  this, METHODID_JOIN)))
          .addMethod(
            getUpdatePNodeMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                cz.fel.grpc.UpdatePNodeRequest,
                cz.fel.grpc.UpdatePNodeResponse>(
                  this, METHODID_UPDATE_PNODE)))
          .addMethod(
            getUpdateNNNodeMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                cz.fel.grpc.UpdateNNNodeRequest,
                com.google.protobuf.Empty>(
                  this, METHODID_UPDATE_NNNODE)))
          .addMethod(
            getMissingNodeMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                cz.fel.grpc.MissingNodeRequest,
                com.google.protobuf.Empty>(
                  this, METHODID_MISSING_NODE)))
          .addMethod(
            getLeaveMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                cz.fel.grpc.LeaveRequest,
                com.google.protobuf.Empty>(
                  this, METHODID_LEAVE)))
          .addMethod(
            getElectionMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                cz.fel.grpc.ElectionRequest,
                com.google.protobuf.Empty>(
                  this, METHODID_ELECTION)))
          .addMethod(
            getElectedMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                cz.fel.grpc.ElectedRequest,
                com.google.protobuf.Empty>(
                  this, METHODID_ELECTED)))
          .addMethod(
            getPublishPublicKeyMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                cz.fel.grpc.PublishPublicKeyRequest,
                com.google.protobuf.Empty>(
                  this, METHODID_PUBLISH_PUBLIC_KEY)))
          .addMethod(
            getSendMessageMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                cz.fel.grpc.SendMessageRequest,
                com.google.protobuf.Empty>(
                  this, METHODID_SEND_MESSAGE)))
          .build();
    }
  }

  /**
   */
  public static final class NodeServiceStub extends io.grpc.stub.AbstractAsyncStub<NodeServiceStub> {
    private NodeServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected NodeServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new NodeServiceStub(channel, callOptions);
    }

    /**
     */
    public void join(cz.fel.grpc.JoinRequest request,
        io.grpc.stub.StreamObserver<cz.fel.grpc.JoinResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getJoinMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updatePNode(cz.fel.grpc.UpdatePNodeRequest request,
        io.grpc.stub.StreamObserver<cz.fel.grpc.UpdatePNodeResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdatePNodeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateNNNode(cz.fel.grpc.UpdateNNNodeRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateNNNodeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void missingNode(cz.fel.grpc.MissingNodeRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getMissingNodeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void leave(cz.fel.grpc.LeaveRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLeaveMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void election(cz.fel.grpc.ElectionRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getElectionMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void elected(cz.fel.grpc.ElectedRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getElectedMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void publishPublicKey(cz.fel.grpc.PublishPublicKeyRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getPublishPublicKeyMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void sendMessage(cz.fel.grpc.SendMessageRequest request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSendMessageMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class NodeServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<NodeServiceBlockingStub> {
    private NodeServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected NodeServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new NodeServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public cz.fel.grpc.JoinResponse join(cz.fel.grpc.JoinRequest request) {
      return blockingUnaryCall(
          getChannel(), getJoinMethod(), getCallOptions(), request);
    }

    /**
     */
    public cz.fel.grpc.UpdatePNodeResponse updatePNode(cz.fel.grpc.UpdatePNodeRequest request) {
      return blockingUnaryCall(
          getChannel(), getUpdatePNodeMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty updateNNNode(cz.fel.grpc.UpdateNNNodeRequest request) {
      return blockingUnaryCall(
          getChannel(), getUpdateNNNodeMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty missingNode(cz.fel.grpc.MissingNodeRequest request) {
      return blockingUnaryCall(
          getChannel(), getMissingNodeMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty leave(cz.fel.grpc.LeaveRequest request) {
      return blockingUnaryCall(
          getChannel(), getLeaveMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty election(cz.fel.grpc.ElectionRequest request) {
      return blockingUnaryCall(
          getChannel(), getElectionMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty elected(cz.fel.grpc.ElectedRequest request) {
      return blockingUnaryCall(
          getChannel(), getElectedMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty publishPublicKey(cz.fel.grpc.PublishPublicKeyRequest request) {
      return blockingUnaryCall(
          getChannel(), getPublishPublicKeyMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.google.protobuf.Empty sendMessage(cz.fel.grpc.SendMessageRequest request) {
      return blockingUnaryCall(
          getChannel(), getSendMessageMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class NodeServiceFutureStub extends io.grpc.stub.AbstractFutureStub<NodeServiceFutureStub> {
    private NodeServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected NodeServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new NodeServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<cz.fel.grpc.JoinResponse> join(
        cz.fel.grpc.JoinRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getJoinMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<cz.fel.grpc.UpdatePNodeResponse> updatePNode(
        cz.fel.grpc.UpdatePNodeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdatePNodeMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> updateNNNode(
        cz.fel.grpc.UpdateNNNodeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateNNNodeMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> missingNode(
        cz.fel.grpc.MissingNodeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getMissingNodeMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> leave(
        cz.fel.grpc.LeaveRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getLeaveMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> election(
        cz.fel.grpc.ElectionRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getElectionMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> elected(
        cz.fel.grpc.ElectedRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getElectedMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> publishPublicKey(
        cz.fel.grpc.PublishPublicKeyRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getPublishPublicKeyMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> sendMessage(
        cz.fel.grpc.SendMessageRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSendMessageMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_JOIN = 0;
  private static final int METHODID_UPDATE_PNODE = 1;
  private static final int METHODID_UPDATE_NNNODE = 2;
  private static final int METHODID_MISSING_NODE = 3;
  private static final int METHODID_LEAVE = 4;
  private static final int METHODID_ELECTION = 5;
  private static final int METHODID_ELECTED = 6;
  private static final int METHODID_PUBLISH_PUBLIC_KEY = 7;
  private static final int METHODID_SEND_MESSAGE = 8;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final NodeServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(NodeServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_JOIN:
          serviceImpl.join((cz.fel.grpc.JoinRequest) request,
              (io.grpc.stub.StreamObserver<cz.fel.grpc.JoinResponse>) responseObserver);
          break;
        case METHODID_UPDATE_PNODE:
          serviceImpl.updatePNode((cz.fel.grpc.UpdatePNodeRequest) request,
              (io.grpc.stub.StreamObserver<cz.fel.grpc.UpdatePNodeResponse>) responseObserver);
          break;
        case METHODID_UPDATE_NNNODE:
          serviceImpl.updateNNNode((cz.fel.grpc.UpdateNNNodeRequest) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_MISSING_NODE:
          serviceImpl.missingNode((cz.fel.grpc.MissingNodeRequest) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_LEAVE:
          serviceImpl.leave((cz.fel.grpc.LeaveRequest) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_ELECTION:
          serviceImpl.election((cz.fel.grpc.ElectionRequest) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_ELECTED:
          serviceImpl.elected((cz.fel.grpc.ElectedRequest) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_PUBLISH_PUBLIC_KEY:
          serviceImpl.publishPublicKey((cz.fel.grpc.PublishPublicKeyRequest) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        case METHODID_SEND_MESSAGE:
          serviceImpl.sendMessage((cz.fel.grpc.SendMessageRequest) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class NodeServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    NodeServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return cz.fel.grpc.NodeServiceProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("NodeService");
    }
  }

  private static final class NodeServiceFileDescriptorSupplier
      extends NodeServiceBaseDescriptorSupplier {
    NodeServiceFileDescriptorSupplier() {}
  }

  private static final class NodeServiceMethodDescriptorSupplier
      extends NodeServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    NodeServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (NodeServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new NodeServiceFileDescriptorSupplier())
              .addMethod(getJoinMethod())
              .addMethod(getUpdatePNodeMethod())
              .addMethod(getUpdateNNNodeMethod())
              .addMethod(getMissingNodeMethod())
              .addMethod(getLeaveMethod())
              .addMethod(getElectionMethod())
              .addMethod(getElectedMethod())
              .addMethod(getPublishPublicKeyMethod())
              .addMethod(getSendMessageMethod())
              .build();
        }
      }
    }
    return result;
  }
}
