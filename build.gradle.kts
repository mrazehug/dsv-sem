import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.google.protobuf.gradle.*;

plugins {
    kotlin("jvm") version "1.7.20"
    application
    idea
    id("com.google.protobuf") version "0.8.18"
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

application {
    mainClass.set("cz.fel.grpc.ChatKt")
}

group = "cz.fel.grpc"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

sourceSets {
    main {
        java {
            srcDirs("build/generated/source/main/grpc")
            srcDirs("build/generated/source/main/grpckt")
            srcDirs("build/generated/source/main/java")
        }
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.7.21")

    implementation("io.grpc:grpc-protobuf:1.51.0")
    implementation("io.grpc:grpc-stub:1.51.0")
    implementation("io.grpc:grpc-netty:1.51.0")
    implementation("io.grpc:grpc-kotlin-stub:1.3.0")

    implementation("org.apache.logging.log4j:log4j-core:2.19.0")
    implementation("org.apache.logging.log4j:log4j-api:2.19.0")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

protobuf {
    protoc{
        artifact = "com.google.protobuf:protoc:3.10.1"
    }
    plugins {
        id("grpc"){
            artifact = "io.grpc:protoc-gen-grpc-java:1.33.1"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:0.1.5"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                id("grpc")
                id("grpckt")
            }
        }
    }
}

tasks {
    test {
        useJUnitPlatform()
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    named<JavaExec>("run") {
        standardInput = System.`in`
    }

    jar {
        manifest {
            attributes["Main-Class"] = application.mainClass
        }
    }

    shadowJar {
        mergeServiceFiles()
        archiveClassifier.set("shadow")
        manifest {
            attributes["Main-Class"] = application.mainClass
            attributes["Multi-Release"] = true
        }
    }
}